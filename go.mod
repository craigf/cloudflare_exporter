module gitlab.com/gitlab-org/cloudflare_exporter

go 1.14

require (
	github.com/go-kit/kit v0.9.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.2.0 // indirect
	github.com/oklog/run v1.1.0
	github.com/prometheus/client_golang v1.4.0
	github.com/prometheus/common v0.9.1
	github.com/stretchr/testify v1.4.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
